const got = require('./data.cjs');

function nameWithS(got) {
    const result = [];
    got.houses.forEach(function (element) {
        element.people.forEach(function (person) {
            const lastname = person.name.split(" ");
            if (lastname[lastname.length - 1].startsWith('A') === true) {
                result.push(person.name);
            }
        });
    });
    return result;
}

console.log(nameWithS(got));