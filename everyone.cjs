const got = require('./data.cjs');

function everyone(got) {
    const result = [];
    got.houses.forEach(function (element) {
        element.people.forEach(function (person) {
            result.push(person.name);
        })
    });
    return result;
}


console.log(everyone(got));