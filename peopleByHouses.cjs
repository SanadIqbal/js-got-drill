const got = require('./data.cjs')

function peopleByHouses(got) {
    let result = {};
    got.houses.forEach(function (element) {
        result[element.name] = element.people.length;
    });

    return result.sort();
}

console.log(peopleByHouses(got));